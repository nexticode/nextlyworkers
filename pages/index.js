import React, { Component } from 'react'
import content from '../content/home.md';

export default class Home extends Component {
  render() {
    let { html, attributes: { title, cats } } = content;
    return (
      <article>
        <h1>{title}</h1>
        <div dangerouslySetInnerHTML={{ __html: html }} />
        <ul>
          {cats.map((cat, k) => (
            <li key={k}>
              <h2>{cat.name}</h2>
              <p>{cat.description}</p>
            </li>
          ))}
        </ul>
        <form method="POST" action="/email">
          Subject: <br></br>
          <input type="text" name="subject" placeholder="The subject" />
          <br></br>
          Name:<br></br>
          <input type="text" name="name" placeholder="Your name" />
          <br></br>
          Email:<br></br>
          <input type="email" name="email" placeholder="Your email" />
          <br></br>
          Message:<br></br>
          <textarea name="message" rows="4"></textarea>
          <br></br>
          <input type="submit" value="Submit"/>
          </form> 
      </article>
    )
  }
}