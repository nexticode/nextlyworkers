# Nextlyworkers

A project integrates `Next.js`, `Netlify`, `NetlifyCMS`, `Cloudflare workers` and `Mailgun` to allow developers to deploy a static website with a CMS 100% free.

 - Netlify ( Free hosting )
 - Cloudflare Workers (100k/day free requests, to handle forms)
 - Mailgun to send emails ( 10k/mo free email )
 - NetlifyCMS (CMS for static sites)

### Install project locally

```
npm install 
```

### Available scripts

```
  "scripts": {
    "dev": "next dev",
    "build": "next build",
    "start": "next start",
    "export": "next export",
    "bundle": "webpack --config webpack.config.js",
    "workers": "serverless deploy --verbose",
    "deploy": "npm run bundle && npm run workers && npm run build && npm run export"
  }
  ```

## Required Environment Vars

You should setup the next list of environment variables in order to be able to deploy the project. Those variable should be set in both Netlify and your local machine if you want to be able to deploy workers from it.

The next list of env variables is required

```
# Cloudflare
export CLOUDFLARE_AUTH_EMAIL={some_email}
export CLOUDFLARE_AUTH_KEY={ api_key }
export CLOUDFLARE_ACCOUNT_ID={account_id }
export CLOUDFLARE_ZONE_ID={ zone_id }
export WORKERS_ROUTE={ your_workers_route }

# Mailgun
export MAILGUN_BASE_API={ api_base_url }
export MAILGUN_API_KEY={ api_key }
```

## Configuring Netlify 

This project uses the Netlify CI/CD to deploy the static version of the site and the Cloudflare workers.

In order to deploy the project, connect your netlify website to your repo. 

Under Build settings : 

```
Repository : {your_reposity_url}
Base directory : Not set
Build command : npm run deploy
Publish directory : out
```

>  Make sure you setup the Publish directory to `out` since that's the default folder Next.js when generating the static pages.

> NOTE : Make sure you setup your environment vars in Netlify under build settings.


## Configuring Cloudflare

Netlify by itself is enough to server your static website with a great performance but performance is not the reason why we are adding cloudflare to this project. We want to take advantage of the 100k free requests per day that Cloudflare Workers offer so we don't need to pay for the Netlify forms. 

In order to add Cloudflare in front of your Netlify server, we need to transfer our domain DNS to Cloudflare.

After configuring your domain NameServers to point to the Cloudflare DNS, you will need to add a CNAME record to your DNS pointing to your netlify domain.

```
CNAME {Your domain} nextlyworkers.netlify.com
```

> This process may take up to 24h due to propagation. Usuallty it's quicker that that.


## Deploy project

You can configure Netlify to deploy your project automatically every time there is a merge with the selected branch (usually master) or you can do manual deployes.

## Workers
You can update the workers directly from your local environment without having to commit your changes to the repo. 

### Deploy workers locally

Make sure that the necessary environment variables are set before you proceed

```
npm run bundle 

&& 

serverless deploy --verbose

```

### Running local website (Next.js)

```
npm run dev
```

A server will be available at [localhost:3000](http://localhost:3000)

## Accessing NetlifyCMS (Only works on deployed version)

This project includes the NetlifyCMS to allow users to manage their website content. Since this is a static site, the path to the CMS admin area is a bit different.

```
{domain}/static/admin
```

> In order to access the admin area you need to activate Netlify identity for the project which allows up to 5 free users.

## Frontend development

This project uses [Tailwind](https://tailwindcss.com) as the CSS framework.

### Tailwind 

Tailwind CSS is a highly customizable, low-level CSS framework that gives you all of the building blocks you need to build bespoke designs without any annoying opinionated styles you have to fight to override.
You can check Taildwind documentation in thier [official website](https://tailwindcss.com/docs/).

### Storybook

This project uses Storybook to allow the development of frontend components in isolation in order to help them deliver robust UIs. Project includes also the viewport plugin pre-configured.

In order to run Storybook run

```
npm run storybook
```
