/**
 * Cloudflare worker email sender
 * Mailgun
 */
addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

function validateEmail(email) {
    // Use a regular expression to decide if an email is valid (true) or not (false)
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
}

function sendResponse(statusText, httpCode, payload) {
    return new Response('{"status" : "' + statusText + '", "data": "' + payload + '"}', {
        status: httpCode,
        headers: new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*' // change to desired cross-origin-policy domain
        })
    })
}

async function handleRequest(request) {
    if (request.method === 'POST') {
        let reqBody = await readRequestBody(request)
        return sendEmail(reqBody);
    }
}

async function readRequestBody(request) {
    const { headers } = request
    const contentType = headers.get('content-type')
    if (contentType.includes('application/json')) {
        const body = await request.json()
        return JSON.stringify(body)
    } else if (contentType.includes('application/text')) {
        const body = await request.text()
        return body
    } else if (contentType.includes('text/html')) {
        const body = await request.text()
        return body
    } else if (contentType.includes('form')) {
        const formData = await request.formData()
        let body = {}
        for (let entry of formData.entries()) {
            body[entry[0]] = entry[1]
        }
        return JSON.stringify(body)
    } else {
        let myBlob = await request.blob()
        var objectURL = URL.createObjectURL(myBlob)
        return objectURL
    }
}

async function sendEmail(reqBody) {

    const body = JSON.parse(reqBody)

    const data = {
        from: 'nuno@nexticode.com',
        to: 'nuno@nexticode.com',
        subject: 'Contact form: ' + body.subject,
        text: 'This is a form submit',
        html: body.name + '<br>' + body.email + '<br>' + body.subject + '<br>'+ body.message + '<br>'
    }

    let formBody = [];
    for (let name in data) {
        formBody.push(encodeURIComponent(name) + "=" + encodeURIComponent(data[name]))
    }

    const mailgunResponse = await fetch(
        process.env.MAILGUN_BASE_API + '/messages' , {
        method: 'POST',
        body: formBody.join("&"),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + Buffer.from("api:" + process.env.MAILGUN_API_KEY).toString('base64')
        }
    })

    try{
        let response = await mailgunResponse

        return sendResponse('success', 200, JSON.stringify(response) )

    }catch(err){
        return sendResponse('error', 400, err)
    }

}