const path = require('path');
const { EnvironmentPlugin } = require("webpack");

module.exports = {
    entry: './workers/mail.js',
    output: {
        filename: 'mail.js',
        path: path.resolve(__dirname, 'webworker'),
    },
    plugins: [
        new EnvironmentPlugin(['MAILGUN_BASE_API', 'MAILGUN_API_KEY'])
    ]
};